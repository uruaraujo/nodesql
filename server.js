const Sequelize = require('sequelize');
const sequelize = new Sequelize('mysql://root:@localhost:3306/acamica');

sequelize.query('SELECT * FROM bandas',{type:sequelize.QueryTypes.SELECT}
).then(function(resultados){
    console.log(resultados);
});

sequelize.query('SELECT * FROM bandas WHERE pais = ? ',
{replacements:['inglaterra'],
type:sequelize.QueryTypes.SELECT}
).then(function(resultados){
    console.log(resultados);
});

sequelize.query('SELECT * FROM bandas WHERE pais =:pais AND id=:id',
{
    replacements:{pais:'USA',id:'3'}, 
    type:sequelize.QueryTypes.SELECT
},
).then(function(resultados){
    console.log(resultados);
});

sequelize.query('DELETE FROM bandas WHERE id=2').then(function(resultados){
    console.log(resultados);
});

sequelize.query('UPDATE bandas SET nombre=:nombre WHERE id=1',
 {
     replacements: {
        nombre: 'Angra'
    }
}).then(function(resultados){
    console.log(resultados);
});

sequelize.query('INSERT INTO bandas(nombre, integrantes, fecha_inicio, pais) VALUES (:nombre,:integrantes,:fecha_inicio,:pais)', {replacements: {
    nombre: 'Metallica', 
    integrantes: 4, 
    fecha_inicio: "1986-6-20", 
    pais: 'USA'
}
}).then(function(resultados){
    console.log(resultados);
}); 